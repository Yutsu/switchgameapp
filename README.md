# Switch Games Website
![Design homepage of the website switch game](./images/homepage.png)

## Description
This is a responsive front-end Switch Games Website project using HTML/CSS and Javascript. With this project, I wanted to show all my switch games that I have.

## Contains
About section animated.

Price range value.

Search filter.

Button genre filter.

Display data width grid and flex.

## Technologies Used
Html

Css

Javascript

Gsap

## Installation
git clone https://gitlab.com/Yutsu/switchgameapp.git

## Start Server
Live server from Vscode Extension and click "Go live"

## Ressources

**Polina Tankilevitch**

https://www.pexels.com/photo/snow-man-people-woman-4523008/

https://www.pexels.com/photo/dirty-internet-connection-technology-4523030/

**Mister Mister**

https://www.pexels.com/photo/person-holding-nintendo-switch-4075410/

Game pictures are from Nintendo