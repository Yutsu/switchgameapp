
const listOfGame = listGame();
listOfGame.sort((a, b) => a.name.localeCompare(b.name));

const container = document.querySelector('.container-games');
const containerAbout = document.querySelector('.container-about');
const allBtn = document.querySelectorAll('button');
const errorSmall = document.querySelector('h5');
let inputSearch = document.getElementById('search');
let inputPrice = document.getElementById('priceInput');
let priceValue = document.querySelector('.priceValue');
let arrow = document.querySelector('.arrow');


// FUNCTIONS
const gsapEvent = () => {
    let TL = gsap.timeline({
        scrollTrigger: { trigger: containerAbout }
    });
    const text = containerAbout.querySelector('.text');
    const image = containerAbout.querySelector('.image');
    TL
    .from(text, 1.3, {x: -300, opacity: 0})
    .from(image, 1.3, {x: 300, opacity: 0 },"-=1.2")
}

const displayGame = (games = listOfGame) => {
    let display = games.map(game => {
        return `
            <div class="game">
                <img class="game-img" src="${game.image}" alt="${game.name}"/>
                <div class="game-text">
                    <div class="text-inline">
                        <h3>${game.name}</h3>
                        <h4>${game.price}€</h4>
                    </div>
                    <div class="line"></div>
                        <div class="date_note">
                            <p class="game-date-note"><u>Date:</u> ${game.date}</p>
                            <p class="game-date-note"><u>Note:</u> ${game.note}/10</p>
                        </div>
                    <p class="game-description">${game.description}</p>

                </div>
            </div>
        `;
    });
    //join => remove ","
    container.innerHTML = display.join("");
};

const displayGenre = (genreNameClick) => {
    const tabGenre = [];
    listOfGame.map(game => {
        game.genres.map(genre => {
            if(genreNameClick === genre){
                tabGenre.push(game);
            }
        });
    });
    displayGame(tabGenre);
}

const priceFilter = () => {
    const value = parseInt(priceInput.value);
    priceValue.textContent = `Value: ${value}€`;
    let filterPrice = listOfGame.filter(game => {
        return game.price <= value;
    });
    displayGame(filterPrice);
}

const research = (e) => {
    const item = e.target.value.trim().toLowerCase();
    let gamesByName = listOfGame.filter(game => {
        return game.name.toLowerCase().includes(item);
    });
    if(gamesByName.length === 0) {
        errorSmall.classList.remove("flex-hide");
        container.classList.add("flex-hide");
    }else {
        errorSmall.classList.add("flex-hide");
        container.classList.remove("flex-hide");
        displayGame(gamesByName);
    }
};

const clickGenreBtn = () => {
    allBtn.forEach(btn => {
        btn.addEventListener('click', (e) => {
            const genreNameClick = e.target.textContent;
            inputSearch.value = "";

            if(!errorSmall.classList.contains('flex-hide')){
                container.classList.remove("flex-hide");
                errorSmall.classList.add("flex-hide");
                displayGenre(genreNameClick);
            }
            else if(genreNameClick !== "All"){
                displayGenre(genreNameClick);
                
            } else {
                displayGame();
            }
        });
    });
}

const navScroll = () => {
    let arrow = document.querySelector('.arrow');
    console.log(arrow)
    window.onscroll = () => {
        const top = window.scrollY;
        console.log(top);
        if (top > 550) {
            arrow.classList.add('scrolled');
        }
        else {
            arrow.classList.remove('scrolled');
        }
    }
}

// CALLS
const app = () => {
    window.addEventListener("DOMContentLoaded",displayGame());
    gsapEvent();
    clickGenreBtn();
    navScroll();
    inputPrice.addEventListener('input', priceFilter);
    inputSearch.addEventListener('input', research);
};
app();
