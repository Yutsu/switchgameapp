const listGame = () => [
    {
        id: 1,
        name: "Pan-Pan",
        price: 0.99,
        image: "./images/panPan.jpg",
        description: "It's an open plain adventure that expands on the concept of environmental narrative storytelling with puzzle solving and exploration.",
        date: "04/09/2017",
        note: 4.5,
        genres: [
           "Puzzle"
        ]
    },
    {
        id: 2,
        name: "Pikmin 3",
        price: 44.99,
        image: "./images/pikmin3.jpg",
        description: "Command different types of Pikmin to strategically overcome obstacles, defeat creatures, and find food for your famished home planet.",
        date: "30/10/2020",
        note: 7.2,
        genres: [
            "Strategy",
            "Adventure"
        ]
    },
    {
        id: 3,
        price: 29.99,
        name: "Phoenix Wright",
        image: "./images/phoenixWright.jpg",
        description: "You take control of a defense attorney who must clear his client, accused of committing murder. During the investigative phases, evidence will have to be gathered.",
        date: "09/04/2019",
        note: 8.4,
        genres: [
            "Puzzle"
        ]
    },
    {
        id: 4,
        price: 39.99,
        name: "Pokemon épée",
        image: "./images/pokemonEpee.jpg",
        description: "The game takes place in the new region of Galar and gives access to three new starting Pokémon: Ouistempo (plant), Flambino (fire) and Larméléon (water).",
        date: "15/11/2019",
        note: 4,
        genres: [
            "Strategy",
            "Adventure"
        ]
    },
    {
        id: 5,
        price: 19.99,
        name: "OkamiHD",
        image: "./images/okamiHD.jpg",
        description: "The title has a very particular graphic aspect, and proposes you to embody the goddess Amaterasu reincarnated in a beautiful white wolf, in a quest to restore life and color in your world, terrorized by numerous enemies who make darkness reign. Fight with a paintbrush, a true extension of the character you play, and use it to advance the story.",
        date: "09/08/2018",
        note: 8.5,
        genres: [
            "Adventure"
        ]
    },
    {
        id: 6,
        price: 44.99,
        name: "Mario Odyssey",
        image: "./images/marioOdyssey.jpg",
        description: "Princess Peach has been kidnapped by Bowser. To save her, Mario leaves the Mushroom Kingdom aboard the Odyssey. Accompanied by Chappy, his living hat, he must travel through different kingdoms and do everything to defeat, once again, the terrible Bowser.",
        date: "27/10/2017",
        note: 7.8,
        genres: [
            "Adventure",
            "Platform",
            "Mario"
        ]
    },
    {
        id: 7,
        price: 0,
        name: "Mario Kart 8 Deluxe",
        image: "./images/marioKart.jpg",
        description: "The player can compete with his friends in different modes and types of cups and has access to 32 circuits with various environments. It is possible to play up to 12 simultaneously online and 4 locally.",
        date: "28/04/2017",
        note: 7,
        genres: [
            "Mario"
        ]
    },
    {
        id: 8,
        price: 44.99,
        name: "Smash",
        image: "./images/Smash.jpg",
        description: "This opus brings together all the fighters who have already appeared in the history of Super Smash Bros. ",
        date: "07/12/2018",
        note: 6.5,
        genres: [
            "Mario"
        ]
    },
    {
        id: 9,
        price: 44.99,
        name: "Ni no kuni",
        image: "./images/ninoKuni.jpg",
        description: "A child named Oliver will try to bring his mother back to life by entering a magical world that reflects another vision of reality. He will be guided by Lumi, a fairy spirit who will teach him how to use his newfound magical powers with the help of a mysterious grimoire. Oliver will also be supported by new allies and will be able to summon pets that will be entirely devoted to him.",
        date: "20/09/2019",
        note: 6.2,
        genres: [
            "Adventure",
            "Strategy"
        ]
    },
    {
        id: 10,
        price: 59.99,
        name: "Animal Crossing",
        image: "./images/animalCrossing.jpg",
        description: "Go and settle on an uninhabited island and create your own little paradise and take the time to develop your community. Start by setting up your camp, and then be free to live your life as you wish. Collect resources, use them to create the items you like and, of course, get to know the many villagers who visit your island.",
        date: "20/03/2020",
        note: 8,
        genres: [
            "Others"
        ]
    },
    {
        id: 11,
        price: 59.99,
        name: "Ring Fit",
        image: "./images/ringFit.jpg",
        description: "Thanks to two accessories provided, the Ring-Con and the leg strap, exercise in the real world to move forward in the adventure.",
        date: "18/10/2019",
        note: 6,
        genres: [
            "Others"
        ]
    },
    {
        id: 12,
        price: 44.99,
        name: "Xenoblade Chronicle",
        image: "./images/xenoblade.jpg",
        description: "Japanese RPG, it relates the war between men and machines, caused by a confrontation between the great gods Bionis and Mekonis. Immobilized as their blades came into contact, their bodies are now home to two rival nations: the Homz and the machines. The hero, Shulk, is a young boy who inherits a powerful weapon called Monado, capable of predicting the future.",
        date: "29/05/2020",
        note: 6,
        genres: [
            "Adventure"
        ]
    },
    {
        id: 13,
        price: 13.99,
        name: "Abzû",
        image: "./images/abzu.jpg",
        description: "You will play a diver who sets out to discover the oceans and discover all the beauty and mystery that surrounds her. Discover hundreds of unique species inspired by real creatures and form a powerful bond with the marine life around you.",
        date: "29/11/2018",
        note: 4.5,
        genres: [
            "Others"
        ]
    },
    {
        id: 14,
        price: 5.99,
        name: "A normal lost phone",
        image: "./images/normalPhone.jpg",
        description: "It's an adventure game that puts you in the shoes of a person who finds a cell phone in the street, so you have to search through messages, photos and other applications to find out what happened to Sam, its missing owner.",
        date: "01/03/2018",
        note: 3,
        genres: [
            "Puzzle"
        ]
    },
    {
        id: 15,
        price: 6.49,
        name: "Gris",
        image: "./images/gris.jpg",
        description: "Gris is a girl full of hope lost in her own world, confronted with a painful experience in her life. Her journey manifested in her dress, which gives new abilities to better navigate her faded reality.",
        date: "13/12/2018",
        note: 6.7,
        genres: [
            "Platform"
        ]
    },
    {
        id: 16,
        price: 17.99,
        name: "Monster Sanctuary",
        image: "./images/monsterSanctuary.jpg",
        description: "Explore its vast lands and enlist the help of your monsters during and outside of battle. Use their unique skills to fly, gallop and slam through environmental puzzles and platforming. Traverse this world in your own way.",
        date: "08/12/2020",
        note: 6.7,
        genres: [
            "Platform",
            "Strategy"
        ]
    },
    {
        id: 17,
        price: 9.99,
        name: "Last day of June",
        image: "./images/lastDayOfJune.jpg",
        description: "Join Carl and June on a dream walk that is about to turn into a nightmare, and try to unlock the sequence of events that can save June's life in this cinematic experience that asks the question, 'What would you do to save the person you love?'",
        date: "16/03/2018",
        note: 3,
        genres: [
            "Adventure",
            "Puzzle"
        ]
    },
    {
        id: 18,
        price: 0.99,
        name: "Agent A",
        image: "./images/AgentA.jpg",
        description: "HQ has just given Agent A (you) a new mission. Ruby La Rouge, a notorious enemy spy and dangerous predator is targeting our secret agents. Your mission is to find and capture her.",
        date: "29/08/2019",
        note: 7,
        genres: [
            "Puzzle"
        ]
    },
    {
        id: 19,
        price: 0.99,
        name: "Old man's journey",
        image: "./images/journey.jpg",
        description: "It's a puzzle-filled, introspective adventure that addresses themes of life, regret, reconciliation and hope. In a sun-drenched, hand-drawn world, embark on an authentic journey of gentle puzzles.",
        date: "20/02/2018",
        note: 6.3,
        genres: [
            "Adventure",
            "Puzzle"
        ]
    },
    {
        id: 20,
        price: 1.99,
        name: "Super Chariot",
        image: "./images/superChariot.jpg",
        description: "Chariot is based on physics and balance. The player must lead his two characters dragging their deceased king through dark and dangerous cellars, in order to find him a tomb and collect treasures.",
        date: "10/05/2018",
        note: 4.7,
        genres: [
            "Adventure"
        ]
    },
    {
        id: 21,
        price: 1.99,
        name: "Hue",
        image: "./images/hue.jpg",
        description: "It's a game in which you modify the world by changing its background color. You explore a dangerous gray world, and discover colorful fragments as you search for your missing mother. When the obstacles are the same color as the background, they disappear, creating exciting new puzzles in a world full of perils, mysteries... and invisible colors.",
        date: "06/06/2019",
        note: 6,
        genres: [
            "Platform",
            "Puzzle"
        ]
    },
    {
        id: 22,
        price: 11.99,
        name: "Céleste",
        image: "./images/celeste.jpg",
        description: "Help Madeline survive her inner demons on Mount Celeste in more than 600 levels over eight chapters in this ultra challenging platformer. Complete hundreds of hand-crafted challenges, uncover all the secrets and unravel the mystery of the mountain. (It's like the game 'Super meat boy').",
        date: "25/01/2018",
        note: "7.3",
        genres: [
            "Platform"
        ]
    },
    {
        id: 23,
        price: 6.79,
        name: "The garden between",
        image: "./images/theGardenBetween.jpg",
        description: "Arina and Frendt, the two protagonists, grow up together and are inseparable. They find themselves in a world of floating islands and gardens, where cause and effect seem to mix. It is in this setting that you will be able to control time, not the characters.",
        date: "20/09/2018",
        note: 6.2,
        genres: [
            "Puzzle"
        ]
    },
    {
        id: 24,
        price: 11.69,
        name: "Tohü",
        image: "./images/Tohu.jpg",
        description: "The title places the player in the shoes of a little girl and her robot Cubus. The two protagonists are in charge of investigating the appearance of a dark creature aiming to cause chaos.",
        date: "28/01/2021",
        note: 6.2,
        genres: [
            "Puzzle",
            "Adventure",
        ]
    },
    {
        id: 25,
        price: 5.59,
        name: "A short hike",
        image: "./images/AShortHike.jpg",
        description: "Hike, climb, and soar through Hawk Peak Provincial Park and experience the peaceful landscape. Follow the marked trails to the top, or explore the park on your own. Along the way, you'll meet other hikers, discover hidden treasures, and enjoy the natural surroundings.",
        date: "18/08/2020",
        note: 6.8,
        genres: [
            "Adventure",
            "Platform"
        ]
    },
    {
        id: 26,
        price: 3.98,
        name: "Jenny Leclue",
        image: "./images/jennyLeclue.jpg",
        description: "It's a game where the player follows the journey of Jenny, a young detective. She lives in the quiet town of Arthurton and decides to investigate after her mother is accused of a murder.",
        date: "26/08/2020",
        note: 6,
        genres: [
            "Puzzle",
            "Adventure",
        ]
    },
    {
        id: 27,
        price: 10.99,
        name: "Summer in Mara",
        image: "./images/summerInMara.jpg",
        description: "The game offers you to take care of an island, where you can grow what you need, and you can arrange it as you wish. When the adventure calls, jump aboard your boat and set off to discover new places and meet new characters on the surrounding islands.",
        date: "16/06/2020",
        note: 6.1,
        genres: [
            "Adventure"
        ]
    },
    {
        id: 28,
        price: 3.24,
        name: "Pikuniku",
        image: "./images/pikuniku.jpg",
        description: "Help the heroes overcome hardships and unmask a sinister plot to start a revolution.",
        date: "24/01/2019",
        note: 6.4,
        genres: [
            "Adventure"
        ]
    },
    {
        id: 29,
        price: 10.49,
        name: "The last campfire",
        image: "./images/theLastCampfire.jpg",
        description: "You travel through a magical world, encountering mysterious creatures and strange ruins. The goal of this journey is to light the last campfire so you can return home.",
        date: "27/08/2020",
        note: 6,
        genres: [
            "Puzzle",
            "Adventure",
            "Platform"
        ]
    },
    {
        id: 30,
        price: 11.99,
        name: "Roki",
        image: "./images/roki.jpg",
        description: "You play Tove, a young woman whose goal is to find her family. On your journey, you'll solve puzzles, make new friends, meet monsters that shouldn't exist and stop the villains from winning.",
        date: "15/10/2020",
        note: 6.1,
        genres: [
            "Puzzle",
            "Adventure",
        ]
    },
    {
        id: 31,
        price: 8.74,
        name: "Baba is you",
        image: "./images/baba.jpg",
        description: "It's a puzzle game in which you can change the rules of the game itself. In each level, the rules come in the form of removable blocks: manipulating them alters the way the level works and causes unexpected interactions. A simple push on a block can turn you into a boulder, change clumps of grass into burning obstacles, and even metamorphose the goal to be reached.",
        date: "13/03/2019",
        note: 7.5,
        genres: [
            "Puzzle",
        ]
    },
    {
        id: 32,
        price: 19.99,
        name: "Snipperclips + DLC",
        image: "./images/snipperclips.jpg",
        description: "You have to cut your characters into different shapes to solve many puzzles.",
        date: "03/03/2017",
        note: 8,
        genres: [
            "Puzzle",
        ]
    },
    {
        id: 33,
        price: 1.75,
        name: "Syberia 1 & 2",
        image: "./images/syberia.jpg",
        description: "It's a point and click game about a New York lawyer, Kate Walker, who is sent to Europe by her firm to buy an automaton factory after the death of its owner. However, an heir is still alive but cannot be found. Set out to find him on a unique journey that takes you to the farthest reaches of Siberia and takes you through atypical landscapes, for an adventure that is just as unusual.",
        date: "20/10/2017",
        note: 7,
        genres: [
            "Adventure"
        ]
    },
    {
        id: 34,
        price: 13.99,
        name: "Good job",
        image: "./images/goodjob.jpg",
        description: "You are an employee of a company whose activity is spread over many sectors. Your role is to complete the tasks you are given without doing too much damage, otherwise the job will not be validated. Alone or in pairs, you'll have to be imaginative to accomplish housework or improbable deliveries.",
        date: "26/03/2020",
        note: 7,
        genres: [
            "Others"
        ]
    },
    {
        id: 35,
        price: 0.99,
        name: "Woodle three",
        image: "./images/woodle.jpg",
        description: "Across eight worlds, your mission is to save the barren planet with the magic water drops.",
        date: "21/12/2017",
        note: 5,
        genres: [
            "Platform",
            "Adventure"
        ]
    },
    {
        id: 36,
        price: 14.99,
        name: "Slay the spire",
        image: "./images/slaythespire.jpg",
        description: "It's a card game to be played solo. You will have to face various enemies and creatures and build your deck as you go along from a selection of cards. You will also be able to collect relics with different powers that will allow you to advance and triumph!",
        date: "06/06/2019",
        note: 7.3,
        genres: [
            "Platform",
            "Adventure"
        ]
    },
    {
        id: 37,
        price: 23.99,
        name: "New Super Lucky's Tale",
        image: "./images/fox.jpg",
        description: "You play Lucky, an ever optimistic and energetic fox hero who must help his sister rescue the Book of Ages from Jinx. The player must face his henchmen through classic platform levels.",
        date: "08/11/2019",
        note: 7.1,
        genres: [
            "Platform",
            "Adventure"
        ]
    },
    {
        id: 38,
        price: 1.80,
        name: "Dadish2",
        image: "./images/dadish2.jpg",
        description: "He’s a dad and a radish, and he’s back in his biggest adventure yet! Help reunite Dadish with his missing kids in this charming and challenging platforming adventure.",
        date: "06/06/2021",
        note: 7,
        genres: [
            "Platform",
            "Adventure"
        ]
    },
    {
        id: 39,
        price: 9.99,
        name: "Syberia 3",
        image: "./images/syberia3.jpg",
        description: "I didn't finish syberia 2 so I will not read and put the resume.",
        date: "18/10/2018",
        note: 6.7,
        genres: [
            "Adventure"
        ]
    },
    {
        id: 40,
        price: 9.99,
        name: "Going Under",
        image: "./images/goingUnder.jpg",
        description: "In a colorful dystopian world, you play as an intern exploited by his boss. Venture into the ruins of old start-ups that have gone bankrupt, kill the monsters there, and take possession of their assets in order to fill the pockets of your boss...",
        date: "24/09/2021",
        note: 6.8,
        genres: [
            "Others"
        ]
    },
]